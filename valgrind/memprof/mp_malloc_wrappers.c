/*--------------------------------------------------------------------*/
/*--- malloc/free wrappers for tracking memory allocations         ---*/
/*---                                         mp_malloc_wrappers.c ---*/
/*--------------------------------------------------------------------*/

/*
   This file is part of MemProf, a heavyweight Valgrind tool for
   memory usage profiling.

   Copyright (C) 2016 Cristina-Gabriela Moraru @ CERN
      cristina.moraru09@gmail.com

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/

// TODO filter libraries

#include "pub_tool_basics.h"
#include "pub_tool_execontext.h"
#include "pub_tool_poolalloc.h"
#include "pub_tool_hashtable.h"
#include "pub_tool_libcbase.h"
#include "pub_tool_libcassert.h"
#include "pub_tool_libcprint.h"
#include "pub_tool_mallocfree.h"
#include "pub_tool_options.h"
#include "pub_tool_replacemalloc.h"
#include "pub_tool_threadstate.h"
#include "pub_tool_tooliface.h"     // Needed for MP_include.h
#include "pub_tool_stacktrace.h"    // For VG_(get_and_pp_StackTrace)
#include "pub_tool_libcproc.h"

#include "mp_include.h"

#define MEMPOOL_DEBUG_STACKTRACE_DEPTH 16

VgHashTable *MP_(sizes_hash) = NULL;
PoolAlloc *MP_(sizes_poolalloc) = NULL;

static long int  addr_comp ( const void* node1, const void* node2 ) {
   SizeExtra *ipe1, *ipe2;

   ipe1 = (SizeExtra*) node1;
   ipe2 = (SizeExtra*) node2;

   if (ipe1->ip < ipe2->ip) return -1;
   if (ipe1->ip > ipe2->ip) return  1;

   return 0;
}

inline void get_stacktrace (ThreadId tid, MP_Record *record) {
	UInt i;
   Addr *addrs = VG_(malloc) ("addrs", stacktrace_depth * sizeof(Addr));

   // VG_(get_and_pp_StackTrace)(tid, stacktrace_depth);

   record->nips = VG_(get_StackTrace)(tid, addrs, stacktrace_depth,
                                  NULL/*array to dump SP values in*/,
                                  NULL/*array to dump FP values in*/,
                                  0/*first_ip_delta*/);
   /* Last 4 items in stacktrace are callers of main and do not present
      interest for the current purpose
   */

   record->nips -= 4;
   record->stack_t = VG_(read_nanosecond_timer)();

   record->ips = VG_(malloc)("ips", record->nips * sizeof(UInt));
   MP_(store_stacktrace)(tid, addrs, record->nips);
   for (i = 0; i < record->nips; i++) {
      record->ips[i] = MP_(get_addr_index)(addrs[i]);
   }

   VG_(free)(addrs);
}

inline void* record_alloc( ThreadId tid, Addr addr, SizeT szB, SizeT alignB,
									MP_AllocKind kind, ULong begin, ULong end) {
	MP_Record record;

   record.allockind = kind;
   record.szB = szB;
   record.tid = tid;
   record.begin_t = begin;
   record.addr = addr;
   record.end_t = end;

   StatHeader.NumRecords++;

   get_stacktrace(tid, &record);

   /* Add the mapping addr-size into hash */
	SizeExtra* se   = VG_(allocEltPA)(MP_(sizes_poolalloc));
	se->ip = record.addr;
	se->szB = szB;
	VG_(HT_add_node)(MP_(sizes_hash), se);

   //print_chunk(record);
   
   MP_(create_alloc_statistic) (tid, &record);

   VG_(free)(record.ips);

   return (void*)record.addr;
}

void* MP_(handle_alloc) ( ThreadId tid,
                       SizeT szB, SizeT alignB,
                       MP_AllocKind kind) {
	Addr addr;
	ULong begin, end;

   begin = VG_(read_nanosecond_timer)();
   addr = (Addr)VG_(cli_malloc)( alignB, szB );
   end = VG_(read_nanosecond_timer)();

   if (! addr) {
      return NULL;
   }

   if (kind == MP_AllocCalloc)
      VG_(memset)((void*)addr, 0, szB);

   record_alloc(tid, addr, szB, alignB, kind, begin, end);

   return (void*)addr;
}


void* MP_(malloc) ( ThreadId tid, SizeT n )
{
   return MP_(handle_alloc)(tid, n, VG_(clo_alignment), MP_AllocMalloc);
}

void* MP_(__builtin_new) ( ThreadId tid, SizeT n )
{
   return MP_(handle_alloc)(tid, n, VG_(clo_alignment), MP_AllocNew);
}

void* MP_(__builtin_vec_new) ( ThreadId tid, SizeT n )
{
   return MP_(handle_alloc)(tid, n, VG_(clo_alignment), MP_AllocNewVec);
}

void* MP_(memalign) ( ThreadId tid, SizeT alignB, SizeT n )
{
   return MP_(handle_alloc)(tid, n, alignB, MP_AllocNew);
}

void* MP_(calloc) ( ThreadId tid, SizeT nmemb, SizeT size)
{
   return MP_(handle_alloc)(tid, nmemb* size, VG_(clo_alignment), MP_AllocCalloc);
}

inline void record_free ( ThreadId tid, void *addr, MP_AllocKind kind,
								  ULong begin, ULong end) {
	MP_Record record;

   record.addr = (Addr)addr;
   record.allockind = kind;
   record.tid = tid;
   record.begin_t = begin;
   record.end_t = end;

   /* Search for the mapping addr-size to determine the size of the
		deallocated chunk
   */
	SizeExtra* se   = VG_(allocEltPA)(MP_(sizes_poolalloc));
	se->ip = record.addr;

	SizeExtra* aux = VG_(HT_gen_lookup) ( MP_(sizes_hash), se, addr_comp);
	if (aux == NULL) {
		VG_(printf)("record_free: ERROR: Addr 0x%08lx not found in hash\n", record.addr);
		VG_(exit)(1);
	}
	else {
		record.szB = aux->szB;
		VG_(HT_remove)(MP_(sizes_hash), record.addr);
		VG_(freeEltPA) (MP_(sizes_poolalloc), aux);
	}
	VG_(freeEltPA) (MP_(sizes_poolalloc), se);

   StatHeader.NumRecords++;

   get_stacktrace(tid, &record);

   //print_chunk(record);

   MP_(create_alloc_statistic) (tid, &record);
   VG_(free)(record.ips);
}

void MP_(handle_free) (ThreadId tid, void *addr, MP_AllocKind kind ) {
   ULong begin, end;

	if (!addr) {
		VG_(printf)("handle_free: ERROR: NULL address 0x%08lx\n", addr);
		VG_(exit)(1);
	}

   begin = VG_(read_nanosecond_timer)();
   VG_(cli_free)( addr );
   end = VG_(read_nanosecond_timer)();

   record_free(tid, addr, kind, begin, end);
}

void MP_(free) ( ThreadId tid, void* p )
{
   MP_(handle_free)(tid, p, MP_AllocFree);
}

void MP_(__builtin_delete) ( ThreadId tid, void* p )
{
   MP_(handle_free)(tid, p, MP_AllocDelete);
}

void MP_(__builtin_vec_delete) ( ThreadId tid, void* p )
{
   MP_(handle_free)(tid, p, MP_AllocDeleteVec);
}

void* MP_(handle_realloc) ( ThreadId tid,
                            Addr old_addr,
                            SizeT new_szB,
                            SizeT alignB) {
   Addr addr;
   ULong begin, end;

   if (!old_addr) {
      return NULL;
   }

   begin = VG_(read_nanosecond_timer)();
   addr = (Addr)VG_(cli_realloc)( old_addr, new_szB );
   end = VG_(read_nanosecond_timer)();

   if (! addr) {
      return NULL;
   }

   record_free(tid, (Addr) old_addr, MP_AllocFree, begin, end);
   record_alloc(tid, addr, new_szB, VG_(clo_alignment),
					 MP_AllocRealloc, begin + 1, end + 1);

   return addr;
}

void* MP_(realloc) ( ThreadId tid, void* p_old, SizeT new_szB )
{
   return MP_(handle_realloc)(tid, p_old, new_szB, VG_(clo_alignment));
}

SizeT MP_(malloc_usable_size) ( ThreadId tid, void* p )
{
   // Not implemented
   return 0;
}