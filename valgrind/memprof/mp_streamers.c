/*--------------------------------------------------------------------*/
/*--- Record printers                                              ---*/
/*---                                         mp_malloc_wrappers.c ---*/
/*--------------------------------------------------------------------*/

/*
   This file is part of SwapCheck, a heavyweight Valgrind tool for
   memory usage profiling.

   Copyright (C) 2016 Cristina-Gabriela Moraru @ CERN
      cristina.moraru09@gmail.com

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/

#include "pub_tool_tooliface.h"
#include "pub_tool_poolalloc.h"
#include "pub_tool_hashtable.h"
#include "pub_tool_stacktrace.h"
#include "pub_tool_mallocfree.h"
#include "pub_tool_replacemalloc.h"
#include "pub_tool_libcbase.h"
#include "pub_tool_libcprint.h"
#include "pub_tool_libcassert.h"
#include "pub_tool_clientstate.h"

#include "mp_include.h"

static int global_ip_index = 0;

#define SERIALIZE_FIELD(field, Type) \
   *((Type*)(((Char*)extended_buffer) + ext_buffer_index)) = sc->field;   \
   ext_buffer_index += sizeof(Type);

#define DESERIALIZE_FIELD(index, field, Type) \
   sc.field = *(Type*)(((HChar*)extended_buffer) + index); \
   index += sizeof(Type);

SizeT calculate_header_size(FileHeader fh) {

   return (4 + fh.CmdLength) * sizeof(HChar) + 
          3 * sizeof(UInt) +
          5 * sizeof(SizeT) +
          4 * sizeof(ULong);
}

void setCmdParams() {
	UInt i;
	StatHeader.CmdLength = (VG_(strlen)(VG_(args_the_exename)) + 1) * sizeof(HChar);

	for (i = 0; i < VG_(sizeXA)( VG_(args_for_client) ); i++) {
		HChar* s = *(HChar**)VG_(indexXA)( VG_(args_for_client), i );
		StatHeader.CmdLength += (VG_(strlen)(s) + 1) * sizeof(HChar);
	}

	StatHeader.CmdLine = VG_(malloc) ("cmdline", StatHeader.CmdLength);

	VG_(strcpy)(StatHeader.CmdLine, VG_(args_the_exename));

	for (i = 0; i < VG_(sizeXA)( VG_(args_for_client) ); i++) {
		HChar* s = *(HChar**)VG_(indexXA)( VG_(args_for_client), i );
		VG_(strcat)(StatHeader.CmdLine, " ");
		VG_(strcat)(StatHeader.CmdLine, s);
	}
}

void create_file_header() {
   StatHeader.key[0] = 'M';
   StatHeader.key[1] = 'P';
   StatHeader.key[2] = 'R';
   StatHeader.key[3] = 'F';

   StatHeader.ToolVersion = 1;
   StatHeader.Compression = 0;
   StatHeader.MaxStacks = stacktrace_depth;
   StatHeader.BucketSize = 0;
   StatHeader.NumBuckets = 0;
   StatHeader.Pid = VG_(getpid)();
   StatHeader.StartTime = 0;   // TODO ???
   StatHeader.StartTimeUtc = 0; // TODO ???
   StatHeader.CompressionHeaderSize = 0;
   setCmdParams();
   StatHeader.HeaderSize = calculate_header_size(StatHeader);
}

void print_file_header() {
   VG_(printf)("Key:\t\t\t\t%s\n", StatHeader.key);
   VG_(printf)("ToolVersion:\t\t\t%u\n", StatHeader.ToolVersion);
   VG_(printf)("Compression:\t\t\t%u\n", StatHeader.Compression);
   VG_(printf)("NumRecords:\t\t\t%lu\n", StatHeader.NumRecords);
   VG_(printf)("MaxStacks:\t\t\t%lu\n", StatHeader.MaxStacks);
   VG_(printf)("BucketSize:\t\t\t%lu\n", StatHeader.BucketSize);
   VG_(printf)("NumBuckets:\t\t\t%lu\n", StatHeader.NumBuckets);
   VG_(printf)("Pid:\t\t\t\t%u\n", StatHeader.Pid);
   VG_(printf)("StartTime:\t\t\t%llu\n", StatHeader.StartTime);
   VG_(printf)("StartTimeUtc:\t\t\t%llu\n", StatHeader.StartTimeUtc);
   VG_(printf)("CompressionHeaderSize:\t\t%llu\n", StatHeader.CompressionHeaderSize);
   VG_(printf)("HeaderSize:\t\t\t%llu\n", StatHeader.HeaderSize);
   VG_(printf)("CmdLine:\t\t\t%s\n", StatHeader.CmdLine);
}

/* debug auxiliary function */

/*
void print_file_header_hexa() {
   int i;

   VG_(printf)("Key:\t\t\t\t");
   for (i = 0; i < 4; i++) {
      VG_(printf)("%02lx", StatHeader.key[i]);
   }

   VG_(printf)("\n");

   VG_(printf)("ToolVersion:\t\t\t%08lx\n", StatHeader.ToolVersion);
   VG_(printf)("Compression:\t\t\t%08lx\n", StatHeader.Compression);
   VG_(printf)("NumRecords:\t\t\t%16lx\n", StatHeader.NumRecords);
   VG_(printf)("MaxStacks:\t\t\t%16lx\n", StatHeader.MaxStacks);
   VG_(printf)("BucketSize:\t\t\t%16lx\n", StatHeader.BucketSize);
   VG_(printf)("NumBuckets:\t\t\t%16lx\n", StatHeader.NumBuckets);
   VG_(printf)("Pid:\t\t\t\t%08lx\n", StatHeader.Pid);
   VG_(printf)("StartTime:\t\t\t%16lx\n", StatHeader.StartTime);
   VG_(printf)("StartTimeUtc:\t\t\t%16lx\n", StatHeader.StartTimeUtc);
   VG_(printf)("CompressionHeaderSize:\t\t%16lx\n", StatHeader.CompressionHeaderSize);
   VG_(printf)("HeaderSize:\t\t\t%16lx\n", StatHeader.HeaderSize);
   VG_(printf)("CmdLine:\t\t\t");
   for (i = 0; i < StatHeader.CmdLength; i++) {
      VG_(printf)("%02lx", StatHeader.CmdLine[i]);
   }
   VG_(printf)("\n");
}
*/

void print_chunk(MP_Record sc) {
   int i;

   VG_(printf)("0x%08lx ", sc.addr);
   VG_(printf)("%lu ", sc.szB);
   VG_(printf)("%u ", sc.allockind);
   VG_(printf)("%u ", sc.nips);
   VG_(printf)("%u ", sc.tid);
   VG_(printf)("%llu ", sc.begin_t);
   VG_(printf)("%llu ", sc.end_t);
   VG_(printf)("%llu : ", sc.stack_t);

   for (i = 0; i < sc.nips; i++) {
      VG_(printf)("%u ", sc.ips[i]);
   }

   VG_(printf)("\n");
}

void dump_buffer() {
   MP_Record sc;
   UInt i = 0;

   DESERIALIZE_FIELD(i, addr, Addr)
   DESERIALIZE_FIELD(i, szB, SizeT)
   DESERIALIZE_FIELD(i, allockind, UShort)
   DESERIALIZE_FIELD(i, nips, UShort)
   DESERIALIZE_FIELD(i, tid, ThreadId)
   DESERIALIZE_FIELD(i, begin_t, ULong)
   DESERIALIZE_FIELD(i, end_t, ULong)
   DESERIALIZE_FIELD(i, stack_t, ULong)

   print_chunk(sc);

}

VgHashTable *MP_(stacktrace_extra_info) = NULL;
PoolAlloc *MP_(ips_poolalloc) = NULL;

static Int index_comparator(const void* n1, const void* n2)
{
   const InstrPointerExtra* ipe1 = *(const InstrPointerExtra *const *)n1;
   const InstrPointerExtra* ipe2 = *(const InstrPointerExtra *const *)n2;

   if (ipe1->index < ipe2->index) return -1;
   if (ipe1->index > ipe2->index) return  1;

   return 0;
}

void dump_stacktrace_extra_info () {

   InstrPointerExtra *ipe;
   HChar buf[20];
   UInt n_items, i, sz;
   
   VgHashNode** ips_extras = VG_(HT_to_array) (MP_(stacktrace_extra_info), &n_items);

   VG_(ssort)((void*)ips_extras, n_items, sizeof(VgHashNode*), index_comparator);

   for (i = 0; i < n_items; i++) { 
      ipe = (InstrPointerExtra*) ips_extras[i];

      VG_(sprintf) (buf, "%d\t0x%08lx\n", ipe->index, ipe->ip);
      sz = VG_(strlen)(buf);
      VG_(write)(hash_log_fd, buf, sz);
   }
}

static long int  addr_comparator ( const void* node1, const void* node2 ) {
   InstrPointerExtra *ipe1, *ipe2;

   ipe1 = (InstrPointerExtra*) node1;
   ipe2 = (InstrPointerExtra*) node2;

   if (ipe1->ip < ipe2->ip) return -1;
   if (ipe1->ip > ipe2->ip) return  1;

   return 0;
}

/*
   Store the IPs from the current stacktrace into a hashtable
   in order to print them more efficiently by their index value
*/
void MP_(store_stacktrace) (ThreadId tid, Addr ips[], UInt n_ips) {
   UInt i;

   for (i = 0; i < n_ips; ++i) {
      InstrPointerExtra* ipe   = VG_(allocEltPA)(MP_(ips_poolalloc));
      ipe->ip = ips[i];
      
      InstrPointerExtra* aux = VG_(HT_gen_lookup) ( MP_(stacktrace_extra_info),
                                                    ipe,
                                                    addr_comparator);
      if (aux == NULL) {
         /* If the current IP is not present in the hashed,
            add the current node
         */
         ipe->index = global_ip_index++;
         VG_(HT_add_node)(MP_(stacktrace_extra_info), ipe);
      }
      else {
         VG_(freeEltPA) (MP_(ips_poolalloc), ipe);
      }
   }
}

void MP_(flush_buffer) () {
   // Flush the buffer into the file + compress
   VG_(write)(stat_log_fd, (HChar*)extended_buffer, ext_buffer_index);
   ext_buffer_index = 0;
//   VG_(memset)(extended_buffer, 0, ext_buffer_sz);
}

UInt MP_(get_addr_index)(Addr addr) {
   InstrPointerExtra ipe;
   ipe.ip = addr;

   InstrPointerExtra* aux = VG_(HT_gen_lookup) (MP_(stacktrace_extra_info),
                                                &ipe,
                                                addr_comparator);
   if (aux == NULL) {
      VG_(printf)("Error: IP 0x%08lx not found in hash\n", addr);
      VG_(exit)(1);
   }

   return aux->index;
}

void MP_(serialize_chunk) (MP_Record *sc) {
   UInt i;

   // ((Addr*)extended_buffer)[ext_buffer_index] = sc->addr;
   // ext_buffer_index += sizeof(Addr); 

   SERIALIZE_FIELD(addr, Addr)
   SERIALIZE_FIELD(szB, SizeT)
   SERIALIZE_FIELD(allockind, UShort)
   SERIALIZE_FIELD(nips, UShort)
   SERIALIZE_FIELD(tid, ThreadId)
   SERIALIZE_FIELD(begin_t, ULong)
   SERIALIZE_FIELD(end_t, ULong)
   SERIALIZE_FIELD(stack_t, ULong)

   for (i = 0; i < sc->nips; i++) {
      *((UInt*)(((HChar*)extended_buffer) + ext_buffer_index)) = 
                                    sc->ips[i];
      ext_buffer_index += sizeof(UInt);
   }
}

void MP_(create_alloc_statistic) (ThreadId tid, MP_Record* sc) {
   UInt remainder;

   remainder = ext_buffer_sz - ext_buffer_index; 
   // TODO check if I can use sizeof
   if (remainder < sizeof(MP_Record) + sc->nips * sizeof(Addr)) {
      MP_(flush_buffer)();
   }

   MP_(serialize_chunk) (sc);
   MP_(flush_buffer)();
}
