include $(top_srcdir)/Makefile.tool.am

EXTRA_DIST = docs/sc-manual.xml docs/sc-tech-docs.xml

#----------------------------------------------------------------------------
# Headers
#----------------------------------------------------------------------------

#pkginclude_HEADERS = \
#	memprof.h

noinst_HEADERS = \
	mp_include.h

#----------------------------------------------------------------------------
# memprof-<platform>
#----------------------------------------------------------------------------

noinst_PROGRAMS  = memprof-@VGCONF_ARCH_PRI@-@VGCONF_OS@
if VGCONF_HAVE_PLATFORM_SEC
noinst_PROGRAMS += memprof-@VGCONF_ARCH_SEC@-@VGCONF_OS@
endif

memprof_SOURCES_COMMON = \
	mp_malloc_wrappers.c \
	mp_streamers.c	\
	mp_main.c

memprof_@VGCONF_ARCH_PRI@_@VGCONF_OS@_SOURCES      = \
	$(memprof_SOURCES_COMMON)
memprof_@VGCONF_ARCH_PRI@_@VGCONF_OS@_CPPFLAGS     = \
	$(AM_CPPFLAGS_@VGCONF_PLATFORM_PRI_CAPS@)
memprof_@VGCONF_ARCH_PRI@_@VGCONF_OS@_CFLAGS       = \
	$(AM_CFLAGS_@VGCONF_PLATFORM_PRI_CAPS@) -O2
memprof_@VGCONF_ARCH_PRI@_@VGCONF_OS@_DEPENDENCIES = \
	$(TOOL_DEPENDENCIES_@VGCONF_PLATFORM_PRI_CAPS@)
memprof_@VGCONF_ARCH_PRI@_@VGCONF_OS@_LDADD        = \
	$(TOOL_LDADD_@VGCONF_PLATFORM_PRI_CAPS@)
memprof_@VGCONF_ARCH_PRI@_@VGCONF_OS@_LDFLAGS      = \
	$(TOOL_LDFLAGS_@VGCONF_PLATFORM_PRI_CAPS@)
memprof_@VGCONF_ARCH_PRI@_@VGCONF_OS@_LINK = \
	$(top_builddir)/coregrind/link_tool_exe_@VGCONF_OS@ \
	@VALT_LOAD_ADDRESS_PRI@ \
	$(LINK) \
	$(memprof_@VGCONF_ARCH_PRI@_@VGCONF_OS@_CFLAGS) \
	$(memprof_@VGCONF_ARCH_PRI@_@VGCONF_OS@_LDFLAGS)

if VGCONF_HAVE_PLATFORM_SEC
memprof_@VGCONF_ARCH_SEC@_@VGCONF_OS@_SOURCES      = \
	$(memprof_SOURCES_COMMON)
memprof_@VGCONF_ARCH_SEC@_@VGCONF_OS@_CPPFLAGS     = \
	$(AM_CPPFLAGS_@VGCONF_PLATFORM_SEC_CAPS@)
memprof_@VGCONF_ARCH_SEC@_@VGCONF_OS@_CFLAGS       = \
	$(AM_CFLAGS_@VGCONF_PLATFORM_SEC_CAPS@) -O2
memprof_@VGCONF_ARCH_SEC@_@VGCONF_OS@_DEPENDENCIES = \
	$(TOOL_DEPENDENCIES_@VGCONF_PLATFORM_SEC_CAPS@)
memprof_@VGCONF_ARCH_SEC@_@VGCONF_OS@_LDADD        = \
	$(TOOL_LDADD_@VGCONF_PLATFORM_SEC_CAPS@)
memprof_@VGCONF_ARCH_SEC@_@VGCONF_OS@_LDFLAGS      = \
	$(TOOL_LDFLAGS_@VGCONF_PLATFORM_SEC_CAPS@)
memprof_@VGCONF_ARCH_SEC@_@VGCONF_OS@_LINK = \
	$(top_builddir)/coregrind/link_tool_exe_@VGCONF_OS@ \
	@VALT_LOAD_ADDRESS_SEC@ \
	$(LINK) \
	$(memprof_@VGCONF_ARCH_SEC@_@VGCONF_OS@_CFLAGS) \
	$(memprof_@VGCONF_ARCH_SEC@_@VGCONF_OS@_LDFLAGS)
endif

# mp_main.c contains the helper function for memprof that get called
# all the time. To maximise performance compile with -fomit-frame-pointer
# Primary beneficiary is x86.
mp_main.o: CFLAGS += -fomit-frame-pointer

#----------------------------------------------------------------------------
# vgpreload_memprof-<platform>.so
#----------------------------------------------------------------------------

noinst_PROGRAMS += vgpreload_memprof-@VGCONF_ARCH_PRI@-@VGCONF_OS@.so
if VGCONF_HAVE_PLATFORM_SEC
noinst_PROGRAMS += vgpreload_memprof-@VGCONF_ARCH_SEC@-@VGCONF_OS@.so
endif

if VGCONF_OS_IS_DARWIN
noinst_DSYMS = $(noinst_PROGRAMS)
endif

#VGPRELOAD_memprof_SOURCES_COMMON = #mp_replace_strmem.c

vgpreload_memprof_@VGCONF_ARCH_PRI@_@VGCONF_OS@_so_SOURCES      =

vgpreload_memprof_@VGCONF_ARCH_PRI@_@VGCONF_OS@_so_CPPFLAGS     = \
	$(AM_CPPFLAGS_@VGCONF_PLATFORM_PRI_CAPS@)
vgpreload_memprof_@VGCONF_ARCH_PRI@_@VGCONF_OS@_so_CFLAGS       = \
	$(AM_CFLAGS_PSO_@VGCONF_PLATFORM_PRI_CAPS@) -O2
vgpreload_memprof_@VGCONF_ARCH_PRI@_@VGCONF_OS@_so_DEPENDENCIES = \
	$(LIBREPLACEMALLOC_@VGCONF_PLATFORM_PRI_CAPS@)
vgpreload_memprof_@VGCONF_ARCH_PRI@_@VGCONF_OS@_so_LDFLAGS      = \
	$(PRELOAD_LDFLAGS_@VGCONF_PLATFORM_PRI_CAPS@) \
	$(LIBREPLACEMALLOC_LDFLAGS_@VGCONF_PLATFORM_PRI_CAPS@)

if VGCONF_HAVE_PLATFORM_SEC
vgpreload_memprof_@VGCONF_ARCH_SEC@_@VGCONF_OS@_so_SOURCES      =

vgpreload_memprof_@VGCONF_ARCH_SEC@_@VGCONF_OS@_so_CPPFLAGS     = \
	$(AM_CPPFLAGS_@VGCONF_PLATFORM_SEC_CAPS@)
vgpreload_memprof_@VGCONF_ARCH_SEC@_@VGCONF_OS@_so_CFLAGS       = \
	$(AM_CFLAGS_PSO_@VGCONF_PLATFORM_SEC_CAPS@) -O2
vgpreload_memprof_@VGCONF_ARCH_SEC@_@VGCONF_OS@_so_DEPENDENCIES = \
	$(LIBREPLACEMALLOC_@VGCONF_PLATFORM_SEC_CAPS@)
vgpreload_memprof_@VGCONF_ARCH_SEC@_@VGCONF_OS@_so_LDFLAGS      = \
	$(PRELOAD_LDFLAGS_@VGCONF_PLATFORM_SEC_CAPS@) \
	$(LIBREPLACEMALLOC_LDFLAGS_@VGCONF_PLATFORM_SEC_CAPS@)
endif

# mp_replace_strmem.c runs on the simulated CPU, and it often appears
# in stack traces shown to the user.  It is built with
# -fno-omit-frame-pointer so as to guarantee robust backtraces on x86,
# on which CFI based unwinding is not the "normal" case and so is
# sometimes fragile.
mp_replace_strmem.o: CFLAGS += -fno-omit-frame-pointer

