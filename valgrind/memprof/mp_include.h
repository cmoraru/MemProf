
/*--------------------------------------------------------------------*/
/*--- A header file for all parts of the MemProf tool.             ---*/
/*---                                                 mp_include.h ---*/
/*--------------------------------------------------------------------*/

/*
   This file is part of MemProf, a heavyweight Valgrind tool for
   swapped memory profiling.

   Copyright (C) 2016 Cristina-Gabriela Moraru @ CERN
      cristina.moraru09@gmail.com

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/

#include "pub_tool_libcfile.h"

#ifndef __MP_INCLUDE_H
#define __MP_INCLUDE_H

#define MP_(str)    VGAPPEND(vgMemProf_,str)

#define MP_MALLOC_DEFAULT_REDZONE_SZB    16

typedef
   enum {
      MP_AllocMalloc    = 0,
      MP_AllocCalloc    = 1,
      MP_AllocRealloc   = 2,
      MP_AllocNew       = 3,
      MP_AllocNewVec    = 4,
      MP_AllocFree      = 5,
      MP_AllocDelete    = 6,
      MP_AllocDeleteVec = 7,
   }
   MP_AllocKind;

/* 
   This describes an allocated heap block.
*/
struct _MP_Record {
   /* Address of allocation (ret by malloc). 32/64 bits - arch dependent */
   Addr                 addr;
   
   /* Size of the allocation. 32/64 bits - arch dependent */
   SizeT                szB;
   
   /* ID of thread who did the allocation. Always 32 bits*/
   ThreadId             tid;
   
   /* Type of allocation. Always 16 bits */
   UShort               allockind;
   
   /* Number of IPs stored in the stacktrace. Always 16 bits*/ 
   UShort               nips;  

   /* Timestamps for begining and end of allocation and end
      of stacktrace generation. Always 64 bits each.
   */
   ULong                begin_t, end_t, stack_t;

   /* IPs from stacktrace. 32 bits each */
   UInt                 *ips;
};

typedef struct _MP_Record MP_Record;

/* Output buffer containing profiling data.
   The content of the buffer can be compressed and stored
   into a file. The bigger the size of the buffer the better
   compression ratio for the output */
extern void *extended_buffer;

/* Size of buffer */
extern UInt ext_buffer_sz;

/* Current index to write in buffer */
extern UInt ext_buffer_index;


extern UInt stacktrace_depth;

struct _FileHeader {
   HChar key[4];        // HEADER MARKER
   UInt ToolVersion;    // Version used to generate
   UInt Compression;    // 0 -no compression 10000x zlib
   SizeT NumRecords;    // Number of records in file
   SizeT MaxStacks;     // Max number of stacks 
   SizeT BucketSize;    // Size of compressed buffer, 0 otherwise
   SizeT NumBuckets;    // Number of buckets in file
   UInt Pid;            // PID of process
   ULong StartTime;     // Start time of process in machine time
   ULong StartTimeUtc;  // Start time of process in UTC
   ULong CompressionHeaderSize;
   ULong HeaderSize;        // Header size varies according to architecture
   SizeT CmdLength;     // Length of command-line
   HChar* CmdLine;      // Commandline string
} __attribute__((packed));

typedef struct _FileHeader FileHeader;

#define COPY_FILE_HEADER(dst, src) \
   UInt i; \
   for (i = 0; i < 4; i++) \
      dst->key[i] = src->key[i]; \
   dst->ToolVersion = src->ToolVersion; \
   dst->Compression = src->Compression; \
   dst->NumRecords = src->NumRecords; \
   dst->MaxStacks = src->MaxStacks; \
   dst->BucketSize = src->BucketSize; \
   dst->NumBuckets = src->NumBuckets; \
   dst->Pid = src->Pid; \
   dst->StartTime = src->StartTime; \
   dst->StartTimeUtc = src->StartTimeUtc; \
   dst->CompressionHeaderSize = src->CompressionHeaderSize; \
   dst->HeaderSize = src->HeaderSize; \
   dst->CmdLength = src->CmdLength; \
   for (i = 0; i < src->CmdLength; i++) \
      dst->CmdLine[i] = src->CmdLine[i];

FileHeader StatHeader;

HChar stat_log_name[30], hash_log_name[30];
const HChar* base_log_name;

typedef struct  _InstrPointerExtra {
   struct _InstrPointer *next;
   Addr ip;
   UInt index;
} InstrPointerExtra;

extern PoolAlloc*    MP_(ips_poolalloc);
extern VgHashTable*  MP_(stacktrace_extra_info);

typedef struct  _SizeExtra {
   struct _InstrPointer *next;
   Addr ip;
   SizeT szB;
} SizeExtra;

extern PoolAlloc*    MP_(sizes_poolalloc);
extern VgHashTable*  MP_(sizes_hash);

UInt stat_log_fd;
UInt hash_log_fd;

/* Memory allocation / deallocation hooks */
void* MP_(malloc)               ( ThreadId tid, SizeT n );
void* MP_(__builtin_new)        ( ThreadId tid, SizeT n );
void* MP_(__builtin_vec_new)    ( ThreadId tid, SizeT n );
void* MP_(memalign)             ( ThreadId tid, SizeT align, SizeT n );
void* MP_(calloc)               ( ThreadId tid, SizeT nmemb, SizeT size1 );
void  MP_(free)                 ( ThreadId tid, void* p );
void  MP_(__builtin_delete)     ( ThreadId tid, void* p );
void  MP_(__builtin_vec_delete) ( ThreadId tid, void* p );
void* MP_(realloc)              ( ThreadId tid, void* p, SizeT new_size );
SizeT MP_(malloc_usable_size)   ( ThreadId tid, void* p );


/* Memory management functions */
void* MP_(handle_alloc) ( ThreadId tid, SizeT szB, SizeT alignB,
                          MP_AllocKind kind);
void MP_(handle_free) (ThreadId tid, void *p, MP_AllocKind kind );

void* MP_(handle_realloc) ( ThreadId tid, Addr old_addr,
                            SizeT new_szB, SizeT alignB);

/* Print summary functions */
void MP_(flush_buffer)(void);
void MP_(store_stacktrace) (ThreadId tid, Addr *ips, UInt n_ips);
void MP_(create_alloc_statistic) (ThreadId tid, MP_Record* sc);
void dump_stacktrace_extra_info (void);
void print_chunk(MP_Record sc);

/* Additional auxiliary functions */
UInt MP_(get_addr_index)(Addr addr);
void MP_(serialize_chunk) (MP_Record *sc);

UInt safe_create_logfile(HChar* name);

void create_file_header(void);
void print_file_header(void);

#endif 