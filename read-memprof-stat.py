
# coding: utf-8

from struct import *
import sys
import matplotlib.pyplot as plt

# ```
# struct _MP_Record {
#    /* Address of allocation (ret by malloc). 32/64 bits - arch dependent */
#    Addr                 addr;
#
#    /* Size of the allocation. 32/64 bits - arch dependent */
#    SizeT                szB;
#
#    /* ID of thread who did the allocation. Always 32 bits*/
#    ThreadId             tid;
#
#    /* Type of allocation. Always 16 bits */
#    UShort               allockind;
#
#    /* Number of IPs stored in the stacktrace. Always 16 bits*/
#    UShort               nips;
#
#    /* Timestamps for begining and end of allocation and end
#       of stacktrace generation. Always 64 bits each.
#    */
#    ULong                begin_t, end_t, stack_t;
#
#    /* IPs from stacktrace. 32 bits each */
#    UInt                 *ips;
# };
# ```

class MP_Record:
    def __init__(self, addr, szB, allockind, nips, tid, begin_t, end_t, stack_t):
        self.addr = addr
        self.szB = szB
        self.tid = tid
        self.allockind = allockind
        self.nips = nips
        self.begin_t = begin_t
        self.end_t = end_t
        self.stack_t = stack_t
        self.ips = []

    def set_ips(self, ips):
        self.ips = ips

    def __str__(self):
        output = "0x0" + format(self.addr, '02x') + " "
        output += str(self.allockind) + " "
        output += str(self.szB) + " "
        output += str(self.tid) + " "
        output += str(self.begin_t) + " "
        output += str(self.end_t) + " "
        output += str(self.stack_t) + " : "
        for i in self.ips:
            output += str(i) + " "

        return output 

#struct _FileHeader {
#    /* HEADER MARKER */
#    HChar key[4];
#
#    /* Version used to generate */
#    UInt ToolVersion;
#
#    /* 0 -no compression 10000x zlib */
#    UInt Compression;
#
#    /* Number of records in file */
#    SizeT NumRecords;
#
#    /* Max number of stacks */
#    SizeT MaxStacks;
#
#    /* Size of compressed buffer, 0 otherwise */
#    SizeT BucketSize;
#
#    /* Number of buckets in file */
#    SizeT NumBuckets;
#
#    /* PID of process */
#    UInt Pid;
#
#    /* Start time of process in machine time */
#    ULong StartTime;
#
#    /* Start time of process in UTC */
#    ULong StartTimeUtc;
#
#    /* */
#    ULong CompressionHeaderSize;
#
#    /* Header size varies according to architecture */
#    ULong HeaderSize;
#
#    /* Length of command-line */
#    SizeT CmdLength;
#
#    /* Command-line string */
#    HChar* CmdLine;
#}

class FileHeader:
    def __init__(self, chr1, chr2, chr3, chr4, ToolVersion, Compression,
                 NumRecords, MaxStacks, BucketSize, NumBuckets, Pid, StartTime,
                 StartTimeUtc, CompressionHeaderSize, HeaderSize, CmdLength):
        self.key = chr1 + chr2 + chr3 + chr4
        self.ToolVersion = ToolVersion
        self.Compression = Compression
        self.NumRecords = NumRecords
        self.MaxStacks = MaxStacks
        self.BucketSize = BucketSize
        self.NumBuckets = NumBuckets
        self.Pid = Pid
        self.StartTime = StartTime
        self.StartTimeUtc = StartTimeUtc
        self.CompressionHeaderSize = CompressionHeaderSize
        self.HeaderSize = HeaderSize
        self.CmdLength = CmdLength
    
    def set_key(self, key):
        self.key = key

    def set_cmdline(self, CmdLine):
        self.CmdLine = CmdLine

    def __str__(self):
        output = "Key\t\t\t\t" + str(self.key) + "\n"
        output += "ToolVersion\t\t\t" + str(self.ToolVersion) + "\n"
        output += "Compression\t\t\t" + str(self.Compression) + "\n"
        output += "NumRecords\t\t\t" + str(self.NumRecords) + "\n"
        output += "MaxStacks\t\t\t" + str(self.MaxStacks) + "\n"
        output += "BucketSize\t\t\t" + str(self.BucketSize) + "\n"
        output += "NumBuckets\t\t\t" + str(self.NumBuckets) + "\n"
        output += "StartTime\t\t\t" + str(self.StartTime) + "\n"
        output += "StartTimeUtc\t\t\t" + str(self.StartTimeUtc) + "\n"
        output += "CompressionHeaderSize\t\t" + str(self.CompressionHeaderSize) + "\n"
        output += "HeaderSize\t\t\t" + str(self.HeaderSize) + "\n"
        output += "CmdLength\t\t\t" + str(self.CmdLength) + "\n"
        output += "CmdLine\t\t\t\t" + str(self.CmdLine) + "\n"
        return output
records = []

def read_logfile(filename,arch=64):
    global records
    pos = 0
    if arch == 64:
        fmt_hdr = '=ccccIIQQQQIQQQQQ'
        fmt     = '=QQHHIQQQ'
    else:
        fmt_hdr = '=ccccIIIIIIIQQQQQ'
        fmt     = '=IIHHIQQQ'

    fmt_hdr_sz  = calcsize(fmt_hdr)
    fmt_sz      = calcsize(fmt)

    f = open(filename,'rb')
    
    # Read the fixed-size part of header
    hdrbuf  = f.read(fmt_hdr_sz)
    data    = unpack(fmt_hdr, hdrbuf)
    header  = FileHeader(*data)

    # Read the variable-size header field
    header.set_cmdline(f.read(header.CmdLength))

    print header

    i = 0
    while i < header.NumRecords:
        recordbuf = f.read(calcsize(fmt))
        data = unpack(fmt, recordbuf)
        record = MP_Record(*data)
        record.set_ips(unpack('i' * record.nips, f.read(4 * record.nips)))
        records.append(record)
        if len(sys.argv) == 3 and sys.argv[2] == "--verbose":
            print record
        i += 1

rec = read_logfile(sys.argv[1])

allocs = []
frees = []

alloctimes = []
lifetimes = []

def filter_mem_access():
    global allocs
    global frees
    global records
    for r in records:
        if r.allockind < 5:
            allocs.append(r)
        else:
            frees.append(r)

def calculate_lifetimes():
    global allocs
    global frees
    global lifetimes
    global sizes

    for ra in allocs:
        for rf in frees:
            if ra.addr == rf.addr:
                alloctimes.append(ra.end_t)
                lifetimes.append(rf.end_t - ra.end_t)
                frees.remove(rf)
                break

filter_mem_access()
calculate_lifetimes()
plt.plot(alloctimes, lifetimes, 'r.')
plt.xlabel("Allocation time (ns)")
plt.ylabel("Lifetime (ns)")
plt.show()
